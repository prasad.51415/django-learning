# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.


class BookAdmin(admin.ModelAdmin):
    list_display = ['book_title','author','price']

class AuthorAdmin(admin.ModelAdmin):
    list_display = ['first_name','last_name','address']




admin.site.register(Book, BookAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Cart)
admin.site.register(CustomProfile)
