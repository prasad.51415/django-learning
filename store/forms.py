from django import forms
from django.contrib.auth.forms import UserCreationForm
from . models import CustomProfile

class CustomProfileForm(forms.ModelForm):
    class Meta:
        model = CustomProfile
        fields = ['dp']


