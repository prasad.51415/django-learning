from django.conf.urls import url,include
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^store/', views.store, name='store'),
    url(r'cart/(\d+)', views.cart, name='cart'),
    url(r'^profile/', views.profile, name='profile'),

]+static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)