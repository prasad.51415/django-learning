# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect
from .models import *
from .forms import *

# Create your views here.
def home(request):
    return render(request, 'home.html')


def store(request):
    obj = Book.objects.all()
    return render(request,'store.html', {'obj':obj})



def cart(request, item_id):
    obj = Book.objects.get(id=item_id)
    return render(request, 'cart.html')


def profile(request):
    if request.user.is_authenticated:
        profile_id = request.user.id
        try:
            user_details = User.objects.get(id=profile_id)
        except:
            msg = "Please Login to view Your Profile"

        try:
            dp_d = CustomProfile.objects.get(user_id=request.user.id)
            return render(request, 'profile.html',{'user_details':user_details, 'dp_d':dp_d} )

        except:
            form = CustomProfileForm()
            return render(request, 'profile.html' , {'user_details':user_details, 'form':form})
    else:
        return redirect('auth_login')


