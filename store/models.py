# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class CustomProfile(models.Model):
    dp = models.ImageField(upload_to='images', default='images/download.jpg')
    user = models.OneToOneField(User)


class Author(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    address = models.CharField(max_length=30)

    def __str__(self):
        return self.first_name


class Book(models.Model):
    book_title = models.CharField(max_length=30)
    author = models.ForeignKey(Author)
    price = models.DecimalField(decimal_places=2,default=0.00, max_digits=10)
    quantity = models.IntegerField()

    def __unicode__(self):
        return '{}{}{}'.format(self.book_title,self.author,self.price)



class Cart(models.Model):
    book = models.ManyToManyField(Book)
    user = models.ForeignKey(User)


    def add_to_cart(self, book_id):
        pass



    def remove_from_cart(self):
        pass